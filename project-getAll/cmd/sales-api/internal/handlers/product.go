package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"project/internal/product"
)

// List method is to get all products from the datbase
func List(w http.ResponseWriter, req *http.Request) {
	getdata := product.GetAllData()
	data, err := json.Marshal(getdata)
	if err != nil {
		fmt.Print("error in getting data from database", err.Error())
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", data)

}
