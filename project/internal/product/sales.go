package product

import (
	"context"
	"fmt"
	"project/internal/platform/database"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/google/uuid"
)

// AddSale records a sales transaction for a single Product.
func AddSale(ctx context.Context, ns NewSale, productID string) (*Sale, error) {
	s := Sale{
		ID:        uuid.New().String(),
		ProductID: productID,
		Quantity:  ns.Quantity,
		Paid:      ns.Paid,
	}
	svc := database.Open()
	tableName := "Sales-Table"
	av, err := dynamodbattribute.MarshalMap(s)
	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(tableName),
	}

	_, err = svc.PutItemWithContext(ctx, input)
	if err != nil {
		fmt.Println(err.Error())

	}

	return &s, nil

}

// ListSales gives all Sales for a Product.
func ListSales(ctx context.Context, productID string) ([]Sale, error) {
	sales := []Sale{}

	if _, err := uuid.Parse(productID); err != nil {
		return nil, ErrInvalidID
	}

	svc := database.Open()

	sess := session.Must(session.NewSession(&database.Open().Config))
	svc = dynamodb.New(sess)
	tableName := "Sales-Table"
	result, err := svc.GetItemWithContext(ctx, &dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"ID": {
				S: aws.String(productID),
			},
		},
	})

	if err != nil {
		fmt.Println(err.Error())

	}
	p := Product{}

	err = dynamodbattribute.UnmarshalMap(result.Item, &p)
	if err != nil {
		panic(fmt.Sprintf("Failed to unmarshal Record, %v", err))
	}

	if p.ID == "" {
		fmt.Println("Could not find name empty")

	}
	return sales, nil
}
