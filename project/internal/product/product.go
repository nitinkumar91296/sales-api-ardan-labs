package product

import (
	"context"
	"fmt"
	"os"
	"project/internal/platform/database"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// Predefined errors identify expected failure conditions.
var (
	// ErrNotFound is used when a specific Product is requested but does not exist.
	ErrNotFound = errors.New("product not found")

	// ErrInvalidID is used when an invalid UUID is provided.
	ErrInvalidID = errors.New("ID is not in its proper form")
)

//List function fetches all produts fron database
func List(ctx context.Context) []Product {
	svc := database.Open()
	sess := session.Must(session.NewSession(&database.Open().Config))
	svc = dynamodb.New(sess)
	tableName := "Products-Table"
	proj := expression.NamesList(expression.Name("ID"), expression.Name("Name"), expression.Name("Cost"), expression.Name("Quantity"))
	expr, err := expression.NewBuilder().WithProjection(proj).Build()
	if err != nil {
		fmt.Println("Got error building expression:")
		fmt.Println(err.Error())
		os.Exit(1)
	}
	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ProjectionExpression:      expr.Projection(),
		TableName:                 aws.String(tableName),
	}
	result, err := svc.ScanWithContext(ctx, params)
	if err != nil {
		fmt.Println("Query API call failed:")
		fmt.Println((err.Error()))
		os.Exit(1)
	}

	items := []Product{}

	for _, i := range result.Items {
		item := Product{}

		err = dynamodbattribute.UnmarshalMap(i, &item)

		if err != nil {
			fmt.Println("Got error in unmarshalling:")
			fmt.Println(err.Error())
			os.Exit(1)
		}
		items = append(items, item)
	}
	return items
}

//Retrieve Handler to insert book into the database
func Retrieve(ctx context.Context, id string) (*Product, error) {

	// b := Product{}
	//b.Name = r.FormValue("name")
	if _, err := uuid.Parse(id); err != nil {
		return nil, ErrInvalidID
	}

	svc := database.Open()

	sess := session.Must(session.NewSession(&database.Open().Config))
	svc = dynamodb.New(sess)
	tableName := "Products-Table"
	result, err := svc.GetItemWithContext(ctx, &dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"ID": {
				S: aws.String(id),
			},
		},
	})

	if err != nil {
		fmt.Println(err.Error())

	}
	p := Product{}

	err = dynamodbattribute.UnmarshalMap(result.Item, &p)
	if err != nil {
		panic(fmt.Sprintf("Failed to unmarshal Record, %v", err))
	}

	if p.ID == "" {
		fmt.Println("Could not find name empty")

	}
	return &p, nil
}

//Create Handler to insert book into the database
func Create(ctx context.Context, np NewProduct) (*Product, error) {
	p := Product{
		ID:       uuid.New().String(),
		Name:     np.Name,
		Cost:     np.Cost,
		Quantity: np.Quantity,
	}
	// p = Product{}
	// json.NewDecoder().Decode(&p)
	svc := database.Open()
	tableName := "Products-Table"
	av, err := dynamodbattribute.MarshalMap(p)
	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(tableName),
	}

	_, err = svc.PutItemWithContext(ctx, input)
	if err != nil {
		fmt.Println(err.Error())

	}

	return &p, nil
}

// Update modifies data about a Product. It will error if the specified ID is
// invalid or does not reference an existing Product.
func Update(ctx context.Context, id string, update UpdateProduct) error {
	p, err := Retrieve(ctx, id)
	if err != nil {
		return err
	}

	svc := database.Open()
	input := &dynamodb.UpdateItemInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{

			":c": {
				S: aws.String(*update.Cost),
			},
			":q": {
				S: aws.String(*update.Quantity),
			},
		},
		TableName: aws.String("Products-Table"),
		Key: map[string]*dynamodb.AttributeValue{
			"ID": {
				S: aws.String(p.ID),
			},
		},
		ReturnValues:     aws.String("UPDATED_NEW"),
		UpdateExpression: aws.String("set Cost= :c,Quantity = :q "),
	}

	_, err = svc.UpdateItemWithContext(ctx, input)
	if err != nil {

		return errors.Wrap(err, "error updating product")
	}

	return nil
}

// Delete removes the product identified by a given ID.
func Delete(ctx context.Context, id string) error {
	if _, err := uuid.Parse(id); err != nil {
		return ErrInvalidID
	}
	svc := database.Open()
	input := &dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"ID": {
				S: aws.String(id),
			},
		},
		TableName: aws.String("Products-Table"),
	}

	_, err := svc.DeleteItem(input)
	if err != nil {
		fmt.Println("Got error calling DeleteItem")
		fmt.Println(err.Error())
		return nil
	}

	return nil
}
