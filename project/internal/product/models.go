package product

//Product is an item we sell.
type Product struct {
	ID       string `json:"ID"`
	Name     string `json:"Name"`
	Cost     string `json:"Cost"`
	Quantity string `json:"Quantity"`
}

// NewProduct is what we require from clients when adding a Product.
type NewProduct struct {
	Name     string `json:"Name" validate:"required"`
	Cost     string `json:"Cost" validate:"gte=0"`
	Quantity string `json:"Quantity" validate:"required"`
}

// UpdateProduct defines what information may be provided to modify an
// existing Product. All fields are optional so clients can send just the
// fields they want changed. It uses pointer fields so we can differentiate
// between a field that was not provided and a field that was provided as
// explicitly blank. Normally we do not want to use pointers to basic types but
// we make exceptions around marshalling/unmarshalling.
type UpdateProduct struct {
	Name     *string `json:"Name"`
	Cost     *string `json:"Cost" validate:"omitempty,gte=0"`
	Quantity *string `json:"Quantity" validate:"omitempty,gte=1"`
}

// Sale represents one item of a transaction where some amount of a product was
// sold. Quantity is the number of units sold and Paid is the total price paid.
// Note that due to haggling the Paid value might not equal Quantity sold *
// Product cost.
type Sale struct {
	ID        string `json:"id"`
	ProductID string `json:"product_id"`
	Quantity  string `json:"quantity"`
	Paid      string `sjson:"paid"`
}

// NewSale is what we require from clients for recording new transactions.
type NewSale struct {
	Quantity string `json:"quantity"`
	Paid     string `json:"paid"`
}
