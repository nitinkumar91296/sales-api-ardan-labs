package handlers

import (
	"log"
	"net/http"

	"project/internal/platform/web"
	"project/internal/product"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"
)

// Product defines all of the handlers related to products. It holds the
// application state needed by the handler methods.
type Product struct {
	Log *log.Logger
}

// List method is to get all products from the datbase
func (p *Product) List(w http.ResponseWriter, req *http.Request) error {
	getdata := product.List(req.Context())
	return web.Respond(req.Context(), w, getdata, http.StatusOK)

}

// Retrieve method is to get all products from the datbase
func (p *Product) Retrieve(w http.ResponseWriter, req *http.Request) error {

	id := chi.URLParam(req, "id")
	getdata, err := product.Retrieve(req.Context(), id)
	if err != nil {
		switch err {
		case product.ErrNotFound:
			return web.NewRequestError(err, http.StatusNotFound)
		case product.ErrInvalidID:
			return web.NewRequestError(err, http.StatusBadRequest)
		default:
			return errors.Wrapf(err, "getting product %q", id)
		}
	}
	return web.Respond(req.Context(), w, getdata, http.StatusOK)
}

// Create decodes the body of a request to create a new product. The full
// product with generated fields is sent back in the response.
func (p *Product) Create(w http.ResponseWriter, req *http.Request) error {
	var np product.NewProduct
	if err := web.Decode(req, &np); err != nil {
		return err
	}

	prod, err := product.Create(req.Context(), np)
	if err != nil {
		return err
	}

	return web.Respond(req.Context(), w, prod, http.StatusCreated)
}

// Update decodes the body of a request to update an existing product. The ID
// of the product is part of the request URL.
func (p *Product) Update(w http.ResponseWriter, r *http.Request) error {
	id := chi.URLParam(r, "id")

	var update product.UpdateProduct
	if err := web.Decode(r, &update); err != nil {
		return errors.Wrap(err, "decoding product update")
	}

	if err := product.Update(r.Context(), id, update); err != nil {
		switch err {
		case product.ErrNotFound:
			return web.NewRequestError(err, http.StatusNotFound)
		case product.ErrInvalidID:
			return web.NewRequestError(err, http.StatusBadRequest)
		default:
			return errors.Wrapf(err, "updating product %q", id)
		}
	}

	return web.Respond(r.Context(), w, nil, http.StatusNoContent)
}

// Delete removes a single product identified by an ID in the request URL.
func (p *Product) Delete(w http.ResponseWriter, r *http.Request) error {
	id := chi.URLParam(r, "id")

	if err := product.Delete(r.Context(), id); err != nil {
		switch err {
		case product.ErrInvalidID:
			return web.NewRequestError(err, http.StatusBadRequest)
		default:
			return errors.Wrapf(err, "deleting product %q", id)
		}
	}

	return web.Respond(r.Context(), w, nil, http.StatusNoContent)
}
