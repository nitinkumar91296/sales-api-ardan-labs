package handlers

import (
	"net/http"

	"project/internal/platform/database"

	"project/internal/platform/web"
)

// Check provides support for orchestration health checks.
type Check struct{}

// Health validates the service is healthy and ready to accept requests.
func (c *Check) Health(w http.ResponseWriter, r *http.Request) error {

	var health struct {
		Status string
	}

	// Check if the database is ready.
	if err := database.Open(); err != nil {
		health.Status = "db not ready"
		return web.Respond(r.Context(), w, health, http.StatusInternalServerError)
	}

	health.Status = "ok"
	return web.Respond(r.Context(), w, health, http.StatusOK)
}
