package handlers

import (
	"log"
	"net/http"
	"project/internal/mid"
	"project/internal/platform/web"
)

// APIRouter constructs an http.Handler with all application routes defined.
func APIRouter(logger *log.Logger) http.Handler {
	// Construct the web.App which holds all routes as well as common Middleware.
	app := web.NewApp(logger, mid.Logger(logger), mid.Errors(logger), mid.Metrics())

	{
		c := Check{}
		app.Handle(http.MethodGet, "/v1/health", c.Health)
	}

	p := Product{
		Log: logger,
	}

	app.Handle(http.MethodGet, "/v1/products", p.List)
	app.Handle(http.MethodGet, "/v1/products/{id}", p.Retrieve)
	app.Handle(http.MethodPost, "/v1/products", p.Create)
	app.Handle(http.MethodPut, "/v1/products/{id}", p.Update)
	app.Handle(http.MethodDelete, "/v1/products/{id}", p.Delete)
	// app.Handle(http.MethodPost, "/v1/products/{id}/sales", p.AddSale)
	// app.Handle(http.MethodGet, "/v1/products/{id}/sales", p.ListSales)
	return app
}
