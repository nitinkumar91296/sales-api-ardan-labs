package handlers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"project/internal/product"

	"github.com/go-chi/chi"
)

// Product defines all of the handlers related to products. It holds the
// application state needed by the handler methods.
type Product struct {
	Log *log.Logger
}

// List method is to get all products from the datbase
func (p *Product) List(w http.ResponseWriter, req *http.Request) {
	getdata := product.List()
	data, err := json.Marshal(getdata)
	if err != nil {
		p.Log.Print("error in getting data from database", err.Error())
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", data)

}

// Retrieve method is to get all products from the datbase
func (p *Product) Retrieve(w http.ResponseWriter, req *http.Request) {

	name := chi.URLParam(req, "name")
	getdata := product.Retrieve(name)
	data, err := json.Marshal(getdata)
	if err != nil {
		p.Log.Print("error in getting data from database", err.Error())
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", data)

}
