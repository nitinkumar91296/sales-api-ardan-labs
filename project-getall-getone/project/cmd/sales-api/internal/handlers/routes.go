package handlers

import (
	"log"
	"net/http"
	"project/internal/platform/web"
)

// APIRouter constructs an http.Handler with all application routes defined.
func APIRouter(logger *log.Logger) http.Handler {
	app := web.NewApp(logger)
	p := Product{
		Log: logger,
	}
	app.Handle(http.MethodGet, "/v1/products", p.List)
	app.Handle(http.MethodGet, "/v1/products/{name}", p.Retrieve)
	return app
}
