package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"project/internal/platform/conf"
	"project/internal/schema"
)

func main() {
	// =========================================================================
	// Configuration

	var cfg struct {
		DB struct {
			User       string `conf:"default:Nitin"`
			Password   string `conf:"default:123,noprint"`
			Host       string `conf:"default:localhost"`
			Name       string `conf:"default:DynamoDB"`
			DisableTLS bool   `conf:"default:false"`
		}
		Args conf.Args
	}

	if err := conf.Parse(os.Args[1:], "Products", &cfg); err != nil {
		if err == conf.ErrHelpWanted {
			usage, err := conf.Usage("Products", &cfg)
			if err != nil {
				log.Fatalf("main : generating usage : %v", err)
			}
			fmt.Println(usage)
			return
		}
		log.Fatalf("error: parsing config: %s", err)
	}

	flag.Parse()

	switch flag.Arg(0) {
	case "migrate":
		schema.OpenDb()
		log.Println("Migrations complete")
		return

	case "seed":
		schema.UsingDb()
		log.Println("Seed data complete")
		return
	}

}
