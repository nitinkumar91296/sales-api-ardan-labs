package product

import (
	"fmt"
	"os"
	"project/internal/platform/database"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

//List ...
func List() []Product {
	svc := database.Open()
	sess := session.Must(session.NewSession(&database.Open().Config))
	svc = dynamodb.New(sess)
	tableName := "Products-Table"
	proj := expression.NamesList(expression.Name("Name"), expression.Name("Cost"), expression.Name("Quantity"))
	expr, err := expression.NewBuilder().WithProjection(proj).Build()
	if err != nil {
		fmt.Println("Got error building expression:")
		fmt.Println(err.Error())
		os.Exit(1)
	}
	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ProjectionExpression:      expr.Projection(),
		TableName:                 aws.String(tableName),
	}
	result, err := svc.Scan(params)
	if err != nil {
		fmt.Println("Query API call failed:")
		fmt.Println((err.Error()))
		os.Exit(1)
	}

	items := []Product{}

	for _, i := range result.Items {
		item := Product{}

		err = dynamodbattribute.UnmarshalMap(i, &item)

		if err != nil {
			fmt.Println("Got error in unmarshalling:")
			fmt.Println(err.Error())
			os.Exit(1)
		}
		items = append(items, item)
	}
	return items
}

//Retrieve Handler to insert book into the database
func Retrieve(name string) *Product {

	// b := Product{}
	//b.Name = r.FormValue("name")
	svc := database.Open()

	sess := session.Must(session.NewSession(&database.Open().Config))
	svc = dynamodb.New(sess)
	tableName := "Products-Table"
	result, err := svc.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"Name": {
				S: aws.String(name),
			},
		},
	})

	if err != nil {
		fmt.Println(err.Error())

	}
	p := Product{}

	err = dynamodbattribute.UnmarshalMap(result.Item, &p)
	if err != nil {
		panic(fmt.Sprintf("Failed to unmarshal Record, %v", err))
	}

	if p.Name == "" {
		fmt.Println("Could not find name empty")

	}
	return &p
}
