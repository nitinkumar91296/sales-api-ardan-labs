package product

//Product is an item we sell.
type Product struct {
	Name     string `json:"Name"`
	Cost     string `json:"Cost"`
	Quantity string `json:"Quantity"`
}
